# encoding: utf-8

from abc import ABC, abstractmethod


class Expr(ABC):
    @abstractmethod
    def accept(self, interpreter):
        pass


class Unary(object):
    def __init__(self, operator, right_expr):
        self.operator = operator
        self.right_expr = right_expr

    def accept(self, interpreter):
        pass


class Binary(object):
    def __init__(self, operator, left_expr, right_expr):
        self.operator = operator
        self.left_expr = left_expr
        self.right_expr = right_expr

    def __repr__(self):
        return '(binary|{operator}|{left_expr}|{right_expr})'.format(
            operator=self.operator, left_expr=self.left_expr, right_expr=self.right_expr
        )

    def accept(self, interpreter):
        pass


class Logical(object):
    def __init__(self, left, right, operator):
        self.left = left
        self.right = right
        self.operator = operator

    def accept(self, interpreter):
        pass


class Grouping(object):
    def __init__(self, expr):
        self.expr = expr

    def accept(self, interpreter):
        pass


class Literal(object):
    def __init__(self, value):
        self.value = value

    def accept(self, interpreter):
        pass


class Variable(object):
    def __init__(self, name):
        self.name = name

    def accept(self, interpreter):
        pass


if __name__ == '__main__':
    print("Expression")
